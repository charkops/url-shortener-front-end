import Vue from 'vue'
import App from './App.vue'
import axios from 'axios';

Vue.prototype.$http = axios;

export const eventBus = new Vue({
  data() {
    return {
      shortenedUrl: '',
      backendPort: '8080',
    }
  },
  computed: {
    backendUrl: function() {
      return 'http://ec2-13-49-77-146.eu-north-1.compute.amazonaws.com:' + this.backendPort + '/api/v1/url/shortener'
    }
  }
});

new Vue({
  el: '#app',
  render: h => h(App)
})
